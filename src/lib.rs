use cpp::cpp;

use qmetaobject::prelude::*;

cpp! {{
#include <QtDebug>
#include <QtCore>
#include <QtQml>
#include <QtQuick>
}}

qrc!(register_resources, "/" {

});


pub fn init() {
    register_resources();
}
