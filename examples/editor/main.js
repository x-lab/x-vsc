var bridge = null;
var editor = null;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

function send(name, value) {
    // console.log("JS: Sending", name, JSON.stringify(value));

    bridge.receive(name, JSON.stringify(value));
}

function receive(name, value) {
    // console.log("JS: Receiving", name, value);
    // console.log("JS: Receiving", name, JSON.parse(value));
    // console.log("JS: Editor", editor);

    var data = JSON.parse(value)

    switch (name) {
      case "value":
          editor.getModel().setValue(data);
          break;
      case "language":
          monaco.editor.setModelLanguage(editor.getModel(), data);
          break;
      case "theme":
          monaco.editor.setTheme(data);
          send("theme", editor._themeService._theme.themeName);
          break;
    default:
          break;
    }
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

new QWebChannel(qt.webChannelTransport, function (channel) {

    bridge = channel.objects.bridge;
    bridge.sendDataChanged.connect(receive);

    require.config({ paths: { 'vs': 'min/vs' } });
    require(['vs/editor/editor.main'], function () {
        editor = monaco.editor.create(document.getElementById('container'), {
            fontFamily: "Dank Mono",
	          scrollBeyondLastLine: false,
            automaticLayout: true
        });

        document.getElementById("container").style.height = "100%";

        editor.onDidChangeModelContent((event) => {
            send("value", editor.getModel().getValue())
        });
        editor.onDidChangeModelLanguage((event) => {
            send("language", event.newLanguage)
        })
        bridge.init();
    });
});

/* Local Variables:   */
/* js-indent-level: 4 */
/* End:               */
