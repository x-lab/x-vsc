import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtWebChannel     1.15
import QtWebEngine      1.15

ApplicationWindow {

    id: window;

    visible: true;

     width: 500;
    height: 500;

    QtObject {
        id: bridge;

        property var contents: '';
        property var language: '';
        property var theme: '';

        signal sendDataChanged(string name, string value);

        function send(name, value) {
            // console.log("QML: sending", name, value);

            sendDataChanged(name, JSON.stringify(value));
        }

        function receive(name, value) {
            // console.log("QML: receiving", name, value);

            switch(name) {
            case "theme":
                theme = value;
                break;
            case "language":
                language = value;
                break;
            case "value":
                contents = value;
                break;
            default:
                break;
            }
        }

        function init() {
            send("language", "python");
        }
    }

    WebEngineView {

        id: view;

        anchors.fill: parent;

        settings.javascriptEnabled: true
        settings.pluginsEnabled: true
        settings.autoLoadImages: true
        settings.showScrollBars: false;

        webChannel: WebChannel {
            id: channel;

            Component.onCompleted: {
                channel.registerObjects({
                    "bridge": bridge,
                });
            }
        }

        url: 'qrc:/examples/editor/main.html';

        onJavaScriptConsoleMessage: (level, message, lineNumber, sourceID) => {
            console.log(message, lineNumber, sourceID);
        }
    }
}
